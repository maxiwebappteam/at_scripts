#!/bin/bash
###########

#############################
# This script check DTS connection
# Requires:
# dts.log for accumulate events related to DTS (events DTS history)
# variable.txt for save global variable
# slack.sh and telegram.sh for send to slack and telegram
#
#############################

# Change diretory to dts.log, slack.sh and telegram.sh
cd /home/xcritical/scripts/at/

### VAR ###
SERVER_NAME="#1 staging crypto"
DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND=1
IS_SEND_TO_SLACK=false
IS_SEND_TO_TELEGRAM=true
IS_CHECK_DTS_CONNECTION=true
MAX_LINES_IN_LOG_FILE=1000
MONITORING_LOG_FILE="/home/xcritical/WebAppServer/logs/web-app-server.log"
#MONITORING_LOG_FILE="./web-app-server.log"
DTS_OWN_LOG_FILE="dts.log"

DTS_UP_SEARCH_PHRASES="isConnected == true"
DTS_DOWN_SEARCH_PHRASES="onConnectionLostListener!!!|tradingAPIConnection == null|isConnected == false|Dts disconnected"

SEARCH_PHRASES="$DTS_UP_SEARCH_PHRASES|$DTS_DOWN_SEARCH_PHRASES"

# Index in variable.txt
# TIME LEFT to send next alert.
# Default is DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
DTS_TIME_LEFT_INDEX="0"
# Below is boolean (is there a mistake now?).
# Default is false
IS_DTS_ALERT_INDEX="1"

DAY=$(date '+%d.%m.%Y %H:%M:%S %:z')
HOST_NAME=$(hostname -A)
ADDITIONAL_INFORMATION="Time: $DAY,
        Server: $SERVER_NAME,
        Host name: $HOST_NAME"

### METODS ###
show_array() {
# For debug
    echo ""
    echo "============================="
    string="$(tr ' \n' '.,' < variable.txt | paste -sd',')"
    array=(`echo $string | sed 's/,/\n/g'`)
    printf '%s, ' "${array[@]}"
    echo ""
    echo "============================="
    echo ""
}

get_variable() {
    string="$(tr ' \n' '.,' < variable.txt | paste -sd',')"
    array=(`echo $string | sed 's/,/\n/g'`)
    result="${array[$1]}"
    echo $result
}

send_to_telegram() {
    if [ $IS_SEND_TO_TELEGRAM = true ]; then
        ./telegram.sh \ "$(printf "${1}" \ )" \

    else
        echo "I don't send to telegram"
    fi
}

send_to_slack() {
    if [ $IS_SEND_TO_SLACK = true ]; then
        ./slack.sh "${1}"
    else
        echo "I don't send to slack"
    fi
}

set_variable() {
    string="$(tr ' \n' '.,' < variable.txt | paste -sd',')"
    array=(`echo $string | sed 's/,/\n/g'`)
    array[$1]=$2
    echo ${array[@]} | sed 's/ /\n/g' > variable.txt
}

cut_lines() {
# This method cut first lines more then $MAX_LINES_IN_LOG_FILE
# Examlpe:
# cut_lines $DTS_OWN_LOG_FILE
    FILE_NAME="${1}"
    LINES=$(wc -l $FILE_NAME | tr -s ' ' | cut -d' ' -f1)
    if [[ $MAX_LINES_IN_LOG_FILE -le $LINES ]]; then
        let DELITE_LINES=$LINES-$MAX_LINES_IN_LOG_FILE+1
        tail -n +$DELITE_LINES $FILE_NAME >> $FILE_NAME.tmp
        mv $FILE_NAME.tmp $FILE_NAME
    fi
}

save_log() {
# This method save TEXT to last row in LOG FILE
# and cut first lines more then $MAX_LINES_IN_LOG_FILE
# Examlpe:
# save_log "TEXT" "$DTS_OWN_LOG_FILE"
    echo "${1}" >> ${2}
    sort ${2} | uniq > ${2}.tmp
    mv ${2}.tmp ${2}
    cut_lines ${2}
}

check_dts_connection() {
    if [[ $(get_variable $DTS_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $DTS_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_DTS_CONNECTION = true ]]; then
        if [[ $(echo -e "$1" | awk "BEGIN{IGNORECASE=1} /$DTS_DOWN_SEARCH_PHRASES/") != "" ]]; then
            if [[ $(get_variable $DTS_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                MESSAGE="*DTS IS DOWN*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $DTS_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $DTS_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $DTS_TIME_LEFT_INDEX) minute to send mesage with DTS report."
                let VAR=$(get_variable $DTS_TIME_LEFT_INDEX)-1
                set_variable $DTS_TIME_LEFT_INDEX $VAR
            fi
            set_variable "$IS_DTS_ALERT_INDEX" "true"
            echo "DOWN: $1"
            save_log "$1" "$DTS_OWN_LOG_FILE"
        elif [[ 'x$(echo -e "$1" | awk "BEGIN{IGNORECASE=1} /$DTS_UP_SEARCH_PHRASES/")' != 'x' ]]; then
            if [[ $(get_variable $IS_DTS_ALERT_INDEX) = "true" ]]; then
                MESSAGE="*DTS IS UP*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $DTS_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable "$IS_DTS_ALERT_INDEX" "false"
            echo "UP: $1"
                save_log "$1" "$DTS_OWN_LOG_FILE"
            fi
        fi
    else
        echo "[$DAY] DTS check is disabled."
    fi
}

check_uniq() {
    OWN_LOG="${1}"
    MONITORING_LOG="${2}"
    grep -E -i -w "$SEARCH_PHRASES" $MONITORING_LOG > lines_of_filtred.tmp
    grep -F -f $OWN_LOG lines_of_filtred.tmp > lines_of_common.tmp
    cat lines_of_common.tmp lines_of_filtred.tmp | sort | uniq -u > lines_unic.tmp
    while IFS="" read -r p || [ -n "$p" ]; do
        check_dts_connection "$p"
    done < lines_unic.tmp
}

### main ###
check_uniq $DTS_OWN_LOG_FILE $MONITORING_LOG_FILE

### remove temporary files ###
rm lines_of_common.tmp
rm lines_of_filtred.tmp
rm lines_unic.tmp

exit 0

