#!/bin/bash
###########

### VAR ###
SQL_FOLDER="/home/xcritical/hsqldb_2.3.3/"
LIMIT_FREE_MEMORY=1024
NORMAL="\033[0m"
YELLOW="\033[0;33m"
CYAN="\033[0;36m"
RED='\033[0;31m'

### MENU ###
echo ""
echo -en "${YELLOW}Choose a script to execute.${NORMAL}\n"
echo -en "${CYAN}=====================${NORMAL}\n"
echo "00 - for ESC"
echo -en "${CYAN}======== DataBase ===${NORMAL}\n"
echo "11 - START  DataBase"
echo "12 - STOP   DataBase"
echo "13 - Status DataBase"
echo -en "${CYAN}========= BackEnd ===${NORMAL}\n"
echo "14 - START  BackEnd"
echo "15 - STOP   BackEnd"
echo "16 - Status BackEnd"
echo -en "${CYAN}=== MicroServices ===${NORMAL}\n"
echo "41 - START  API"
echo "42 - STOP   API"
echo "43 - Status API"
echo "44 - START  Autochatist"
echo "45 - STOP   Autochartist"
echo "46 - Status Autochartist"
echo "47 - START  Pechkin"
echo "48 - STOP   Pechkin"
echo "49 - Status Pechkin"
echo -en "${CYAN}=====================${NORMAL}\n"
read SCRIPT_VAR

clear
echo ""

### METODS ###
status_database() {
    pid=$(ps aux | grep hsqldb | grep java | tr -s ' ' | cut -d' ' -f2)
    if [ -z "$pid" ]; then
        echo "DataBase is stopped"
    else
        echo "DataBase is running. pid = $pid"
    fi
}

start_database() {
    pid=$(ps aux | grep hsqldb | grep java | tr -s ' ' | cut -d' ' -f2)
    if [ -z "$pid" ]; then
        FREE_MEMORY=$(free -m | grep 'Mem' | awk ' {print $4}')
#        if [[ $FREE_MEMORY -le $LIMIT_FREE_MEMORY ]]; then
#<----->    echo "Free memory $FREE_MEMORY less then $LIMIT_FREE_MEMORY."
#        else
            cd $SQL_FOLDER
            ./hsqldb.sh -detached
            #sudo
            status_database
#        fi
    else
        echo "DataBase is running. pid = $pid"
    fi
}

kill_database() {
    PID_HSQLDB=$(ps aux | grep hsqldb | grep xcritic | awk ' {print$2}')
    kill -9 $PID_HSQLDB
    status_database
}

status_backend() {
    pid=$(ps aux | grep [w]ebapp\.server\.ServerMain | tr -s ' ' | cut -d' ' -f2)
    if [ -z "$pid" ]; then
        echo "Server is stopped"
    else
        echo "Server is running. pid = $pid"
    fi
}

start_backend() {
    pid=$(ps aux | grep [w]ebapp\.server\.ServerMain | tr -s ' ' | cut -d' ' -f2)
    if [ -z "$pid" ]; then
        cd /home/xcritical/WebAppServer/
        ./bin/server-start.sh
        status_backend
    else
        echo "Server is running. pid = $pid"
    fi
}

kill_bakend() {
    PID_SERVER_MAIN=$(ps aux | grep ServerMain | grep xcritic | awk ' {print $2}')
    kill -9 $PID_SERVER_MAIN
    status_backend
}

status_pechkin() {
    PID_PECHKIN=$(ps aux | grep [m]icroservices\.pechkin\.PechkinServer | tr -s ' ' | cut -d' ' -f2)
    if [[ -z "$PID_PECHKIN" ]]; then
        echo "Pechkin is stopped"
    else
        echo "Pechkin is running. pid = $PID_PECHKIN"
    fi
}

kill_pechkin() {
    kill -9 $(ps aux | grep [m]icroservices\.pechkin\.PechkinServer | tr -s ' ' | cut -d' ' -f2)
    status_pechkin
}

start_pechkin() {
    PID_PECHKIN=$(ps aux | grep [m]icroservices\.pechkin\.PechkinServer | tr -s ' ' | cut -d' ' -f2)
    if [[ -z "$PID_PECHKIN" ]]; then
        cd /home/xcritical/Microservices/pechkin
        ./run-pechkin.sh
        status_pechkin
    else
        echo "Pechkin is running. pid = ${PID_PECHKIN}"
    fi
}

status_autochartist() {
    PID_AUTOCHARTIST=$(ps aux | grep [m]icroservices\.autochartist\.AutochartistServer | tr -s ' ' | cut -d' ' -f2)
    if [[ -z "$PID_AUTOCHARTIST" ]]; then
        echo "Autochartist is stopped"
    else
        echo "Autochartist is running. pid = ${PID_AUTOCHARTIST}"
    fi
}

kill_autochartist() {
    kill -9 $(ps aux | grep [m]icroservices\.autochartist\.AutochartistServer | tr -s ' ' | cut -d' ' -f2)
    status_autochartist
}

start_autochartist() {
    PID_AUTOCHARTIST=$(ps aux | grep [m]icroservices\.autochartist\.AutochartistServer | tr -s ' ' | cut -d' ' -f2)
    if [[ -z "$PID_AUTOCHARTIST" ]]; then
        cd /home/xcritical/Microservices/autochartist
        ./run-autochartist.sh
        status_autochartist
    else
        echo "Autochartist is running. pid = ${PID_AUTOCHARTIST}"
    fi
}

status_api() {
    PID_API=$(ps aux | grep [m]icroservices\.api\.ApiServer | tr -s ' ' | cut -d' ' -f2)
    if [[ -z "$PID_API" ]]; then
        echo "API is stopped"
    else
        echo "API is running. pid = ${PID_API}"
    fi
}

kill_api() {
    kill -9 $(ps aux | grep [m]icroservices\.api\.ApiServer | tr -s ' ' | cut -d' ' -f2)
    status_api
}

start_api() {
    PID_API=$(ps aux | grep [m]icroservices\.api\.ApiServer | tr -s ' ' | cut -d' ' -f2)
    if [[ -z "$PID_API" ]]; then
        cd /home/xcritical/Microservices/api
        ./run-api.sh
        status_api
    else
        echo "API is running. pid = ${PID_API}"
    fi
}

### MENU LOGICS ###
if [[ $SCRIPT_VAR -eq 11 ]]; then
    start_database
elif [[ $SCRIPT_VAR -eq 12 ]]; then
    kill_database
elif [[ $SCRIPT_VAR -eq 13 ]]; then
    status_database
elif [[ $SCRIPT_VAR -eq 14 ]]; then
    start_backend
elif [[ $SCRIPT_VAR -eq 15 ]]; then
    kill_bakend
elif [[ $SCRIPT_VAR -eq 16 ]]; then
    status_backend
elif [[ $SCRIPT_VAR -eq 41 ]]; then
    start_api
elif [[ $SCRIPT_VAR -eq 42 ]]; then
    kill_api
elif [[ $SCRIPT_VAR -eq 43 ]]; then
    status_api
elif [[ $SCRIPT_VAR -eq 44 ]]; then
    start_autochartist
elif [[ $SCRIPT_VAR -eq 45 ]]; then
    kill_autochartist
elif [[ $SCRIPT_VAR -eq 46 ]]; then
    status_autochartist
elif [[ $SCRIPT_VAR -eq 47 ]]; then
    start_pechkin
elif [[ $SCRIPT_VAR -eq 48 ]]; then
    kill_pechkin
elif [[ $SCRIPT_VAR -eq 49 ]]; then
    status_pechkin
else
    echo "You choose ESC"
fi

if [[ $SCRIPT_VAR != "00" ]]; then
    /home/xcritical/scripts/at/menu_xcritical.sh
fi


