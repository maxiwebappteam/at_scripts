#!/bin/bash
###########

### VAR ###
LIMIT_FREE_MEMORY=1024
NORMAL="\033[0m"
YELLOW="\033[0;33m"
CYAN="\033[0;36m"
RED='\033[0;31m'

### MENU ###
echo ""
echo -en "${YELLOW}Choose a script to execute.${NORMAL}\n"
echo -en "${CYAN}=====================${NORMAL}\n"
echo "0 - for ESC"
echo "1 - REBOOT"
echo -en "${CYAN}=====================${NORMAL}\n"
echo "2 - START  RabbitMQ"
echo "3 - STOP   RabbitMQ"
echo "4 - Status RabbitMQ"
echo -en "${CYAN}=====================${NORMAL}\n"
echo "5 - START  NGINX"
echo "6 - STOP   NGINX"
echo "7 - Status NGINX"
echo -en "${CYAN}=====================${NORMAL}\n"
read SCRIPT_VAR

clear
echo ""

### METODS ###
status_nginx() {
    pid=$(ps aux | grep usr/sbin/nginx | grep master | tr -s ' ' | cut -d' ' -f2)
    if [ -z "$pid" ]; then
        echo "NGINX is stopped"
    else
        echo "NGINX is running. pid = $pid"
    fi
}

start_nginx() {
    pid=$(ps aux | grep usr/sbin/nginx | grep master | tr -s ' ' | cut -d' ' -f2)
    if [ -z "$pid" ]; then
        sudo systemctl start nginx
        status_nginx
    else
        echo "NGINX is running. pid = "$pid
    fi
}

kill_nginx() {
    sudo systemctl stop nginx
    status_nginx
}

status_rabbit() {
    pid=$(ps aux | grep etc/rabbitmq | grep rabbitmq_server | tr -s ' ' | cut -d' ' -f2)
    if [ -z "$pid" ]; then
        echo "RabbitMQ is stopped"
    else
        echo "RabbitMQ is running. pid = $pid"
    fi
}

start_rabbit() {
    pid=$(ps aux | grep etc/rabbitmq | grep rabbitmq_server | tr -s ' ' | cut -d' ' -f2)
    if [ -z "$pid" ]; then
        sudo service rabbitmq-server start
        status_rabbit
    else
        echo "RabbitMQ is running. pid = "$pid
    fi
}

kill_rabbitmq() {
    sudo service rabbitmq-server stop
    status_rabbit
}

reboot_question() {
echo -en "${RED}Are you sure to reboot this server? (y/n)${NORMAL}\n"
read REBOOT_ANSWER
    if [[ $REBOOT_ANSWER = "y" ]]; then
        echo "You choose YES. Server will reboot after 3 second"
        sleep 3
        sudo shutdown -r now
    else
        echo "You choose NO"
    fi
}

### MENU LOGICS ###
if [[ $SCRIPT_VAR -eq 1 ]]; then
    reboot_question
elif [[ $SCRIPT_VAR -eq 2 ]]; then
    start_rabbit
elif [[ $SCRIPT_VAR -eq 3 ]]; then
    kill_rabbitmq
elif [[ $SCRIPT_VAR -eq 4 ]]; then
    status_rabbit
elif [[ $SCRIPT_VAR -eq 5 ]]; then
    start_nginx
elif [[ $SCRIPT_VAR -eq 6 ]]; then
    kill_nginx
elif [[ $SCRIPT_VAR -eq 7 ]]; then
    status_nginx
else
    echo "You choose ESC"
fi

if [[ $SCRIPT_VAR != "0" ]]; then
    /home/centos/scripts/at/menu_root.sh
fi

