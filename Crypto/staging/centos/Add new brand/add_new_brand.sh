#!/bin/bash
###########

### VAR ###
NAME=$1
DOMAIN=$2
PREFIX=$3
CRM_API=$4
DAILY_CHANGES_ID=$5
TYPE=$6
FILE=/home/centos/scripts/at/Temp/trading-webapp-server.properties
CURENT_COUNT=$(grep brand.count= ${FILE} | tr -s '=' | cut -d'=' -f2)
COUNT=$(expr $CURENT_COUNT + 1)
ROW_COUNT=0

### METODS ###
add_brand() {
printf "
brand.${COUNT}.name=$NAME
brand.${COUNT}.domain=$DOMAIN
brand.${COUNT}.prefix=$PREFIX
brand.${COUNT}.crmAPI=$CRM_API
brand.${COUNT}.daily_changes_id=$DAILY_CHANGES_ID
brand.${COUNT}.type=$TYPE

" >> ${FILE}
}

add_row() {
    if [[ "$1" = "brand.count=${CURENT_COUNT}" ]]; then
        printf "brand.count=${COUNT}
" >> ${FILE}
    elif [[ "$1" == *"brand.${CURENT_COUNT}."* ]]; then
        ROW_COUNT=$(expr $ROW_COUNT + 1)
        printf "$1
" >> ${FILE}
    elif [[ "$ROW_COUNT" -eq "6" ]]; then
        ROW_COUNT=0
        add_brand
    else
        printf "$1
" >> ${FILE}
    fi
}

### MAIN ###
readarray ARRAY < ${FILE}
printf "" > ${FILE}
for i in ${!ARRAY[@]}; do
  add_row ${ARRAY[$i]}
done

exit 0
