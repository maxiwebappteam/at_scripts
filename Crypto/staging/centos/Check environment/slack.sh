#!/bin/bash
###########

### VAR ###
# It's for slack log chat https://xcriticalcentoslogs.slack.com/archives/C01JGCVCTL1
# $1 - message in double quotes
# $2 - APP_SLACK_WEBHOOK in double quotes

MESSAGE=$1
APP_SLACK_WEBHOOK="https://hooks.slack.com/services/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"
APP_SLACK_ICON_EMOJI=${APP_SLACK_ICON_EMOJI:-:slack:}
APP_SLACK_USERNAME=${APP_SLACK_USERNAME:-$(hostname | cut --delimiter=. --fields=1)}

send_message() {
    curl --silent --data-urlencode \
    "$(printf 'payload={"text": "%s", "channel": "%s", "username": "%s", "as_user": "true", "link_names": "true", "icon_emoji": "%s" }' \
    "${MESSAGE}" \
    "${APP_SLACK_WEBHOOK}" \
    "${APP_SLACK_USERNAME}" \
    "${APP_SLACK_ICON_EMOJI}" \
    )" \
    ${APP_SLACK_WEBHOOK} || true
    echo
}

if [[ $2 != "" ]]; then
    APP_SLACK_WEBHOOK=$2
fi

if [[ $3 != "" && $4 != "" ]]; then
    APP_SLACK_ICON_EMOJI=$3
    APP_SLACK_USERNAME=$4
fi

if [[ "x$MESSAGE" != "x" ]]; then
    send_message
fi
echo ""

exit 0
