]
#!/bin/bash
###########

### VAR ###
# It's for chat t.me/xcritical_log_bot
# xcritical_centos_bot
# $1 - message in double quotes
# $2 - CHAT ID in double quotes
# $3 - USER ID in double quotes

TEXT=$1
CHAT_ID=""
USER_ID=""

if [[ $2 != "" && $3 != "" ]]; then
    CHAT_ID=$2
    USER_ID=$3
else
    # Trading server team
    CHAT_ID="-326403186"

# Igor    CHAT_ID="322825162"
# Oleg    CHAT_ID="269242503"
    USER_ID="1538060804:AAFleEh_p-v5P1_MEBrF275scPibtVrh3Ws"
fi

if [[ "x$TEXT" != "x" ]]; then
    curl -s -X POST https://api.telegram.org/bot$USER_ID/sendMessage -d chat_id=$CHAT_ID -d text="$TEXT"
fi
echo "==================== Send to telegram"

exit 0
