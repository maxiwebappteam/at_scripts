#!/bin/bash
##########################################
# This script need install "sysstat", also
# if you will change DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND, please set
# time left index in variable.txt to also (see index map below).
# Default TIME LEFT = 15 (minutes)
# Default ALERT = false
# Also need ./slack.sh and ./telegram.sh
# Don't forget change server name
##########################################

# Change diretory to slack.sh and telegram.sh
cd /home/centos/scripts/at/

### VAR ###
SERVER_NAME="#1 staging crypto"
DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND=15
LIMIT_MEMORY=1024
LIMIT_DISK_SPACE=2048
LIMIT_CPU_LOAD_AVERAGE=95
HOW_MANY_SECONDS_TO_COLLECT_CPU_AVERAGE=30

IS_SEND_TO_SLACK=true
IS_SEND_TO_TELEGRAM=true

IS_CHECK_MEMORY=true
IS_CHECK_DISK_SPPACE=true
IS_CHECK_CPU=false
IS_CHECK_DATABASE=true
IS_CHECK_BACKEND=true
IS_CHECK_PECHKIN=true
IS_CHECK_AUTOCHARTIST=true
IS_CHECK_API=true
IS_CHECK_RABBIT=true
IS_CHECK_NGINX=true

# Index in variable.txt
# TIME LEFT to send next alert.
# Default is DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
MEMORY_TIME_LEFT_INDEX="0"
FREE_SPACE_TIME_LEFT_INDEX="1"
CPU_TIME_LEFT_INDEX="2"
DATABASE_TIME_LEFT_INDEX="3"
BACKEND_TIME_LEFT_INDEX="4"
PECHKIN_TIME_LEFT_INDEX="5"
AUTOCHARTIST_TIME_LEFT_INDEX="6"
API_TIME_LEFT_INDEX="7"
RABBIT_TIME_LEFT_INDEX="8"
NGINX_TIME_LEFT_INDEX="9"
# Below is boolean (is there a mistake now?).
# Default is false
IS_MEMORY_ALERT_INDEX="10"
IS_FREE_SPACE_ALERT_INDEX="11"
IS_CPU_ALERT_INDEX="12"
IS_DATABASE_ALERT_INDEX="13"
IS_BACKEND_ALERT_INDEX="14"
IS_PECHKIN_ALERT_INDEX="15"
IS_AUTOCHARTIST_ALERT_INDEX="16"
IS_API_ALERT_INDEX="17"
IS_RABBIT_ALERT_INDEX="18"
IS_NGINX_ALERT_INDEX="19"

DAY=$(date '+%d.%m.%Y %H:%M:%S %:z')
HOST_NAME=$(hostname -A)
ADDITIONAL_INFORMATION="Time: $DAY,
        Server: $SERVER_NAME,
        Host name: $HOST_NAME"


### METODS ###
show_array() {
# For debug
    echo ""
    echo "============================="
    string="$(tr ' \n' '.,' < variable.txt | paste -sd',')"
    array=(`echo $string | sed 's/,/\n/g'`)
    printf '%s, ' "${array[@]}"
    echo ""
    echo "============================="
    echo ""
}

get_variable() {
    string="$(tr ' \n' '.,' < variable.txt | paste -sd',')"
    array=(`echo $string | sed 's/,/\n/g'`)
    result="${array[$1]}"
    echo $result
}

set_variable() {
    string="$(tr ' \n' '.,' < variable.txt | paste -sd',')"
    array=(`echo $string | sed 's/,/\n/g'`)
    array[$1]=$2
    echo ${array[@]} | sed 's/ /\n/g' > variable.txt
}

send_to_telegram() {
    if [ $IS_SEND_TO_TELEGRAM = true ]; then
        ./telegram.sh \ "$(printf "${1}" \ )" \

    else
        echo "I don't send to telegram"
    fi
}

send_to_slack() {
    if [ $IS_SEND_TO_SLACK = true ]; then
        ./slack.sh "${1}"
    else
        echo "I don't send to slack"
    fi
}

check_memory() {
    if [[ $(get_variable $MEMORY_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $MEMORY_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_MEMORY = true ]]; then
#        FREE_MEMORY=$(free -m | grep 'Mem:' | awk ' {print $4}')
        TOTAL_MEMORY=$(free -m | grep 'Mem:' | awk ' {print $2}')
        USED_MEMORY=$(free -m | grep 'Mem:' | awk ' {print $3}')
        let FREE_MEMORY=$TOTAL_MEMORY-$USED_MEMORY
        if [[ $LIMIT_MEMORY -gt $FREE_MEMORY ]]; then
            if [[ $(get_variable $MEMORY_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                MESSAGE_MEMORY="*NOT ENOUGH FREE MEMORY*,
        Limit memory: $LIMIT_MEMORY Mb,
        Free memory: $FREE_MEMORY Mb,
        $ADDITIONAL_INFORMATION"
                send_to_telegram "${MESSAGE_MEMORY}"

                send_to_slack "$MESSAGE_MEMORY"
                set_variable $MEMORY_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $MEMORY_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $MEMORY_TIME_LEFT_INDEX) minute to send mesage with memory report."
                let VAR=$(get_variable $MEMORY_TIME_LEFT_INDEX)-1
                set_variable $MEMORY_TIME_LEFT_INDEX $VAR
            fi
            set_variable $IS_MEMORY_ALERT_INDEX "true"
        else
            if [[ $(get_variable $IS_MEMORY_ALERT_INDEX) = "true" ]]; then
                MESSAGE_MEMORY="*ENOUGH FREE MEMORY*,
        Limit memory: $LIMIT_MEMORY Mb,
        Free memory: $FREE_MEMORY Mb,
        $ADDITIONAL_INFORMATION"
                send_to_telegram "${MESSAGE_MEMORY}"

                send_to_slack "$MESSAGE_MEMORY"
                set_variable $MEMORY_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable $IS_MEMORY_ALERT_INDEX "false"
            else
                echo "[$DAY] Enough free memory. Free memory: $FREE_MEMORY Mb. Limit: $LIMIT_MEMORY Mb."
            fi
        fi
    else
        echo "[$DAY] Memory check is disabled."
    fi
}

check_free_space() {
    if [[ $(get_variable $FREE_SPACE_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $FREE_SPACE_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_DISK_SPPACE = true ]]; then
        FREE_DISK_SPACE=$(($(df | sed -n '/^\/dev/p'| awk ' {print $4}') / 1024))
        if [[ $LIMIT_DISK_SPACE -gt $FREE_DISK_SPACE ]]; then
            if [[ $(get_variable $FREE_SPACE_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                MESSAGE_DISK_SPACE="*NOT ENOUGH FREE DISK SPACE*,
        Limit disk space: $LIMIT_DISK_SPACE Mb,
        Free disk space: $FREE_DISK_SPACE Mb,
        $ADDITIONAL_INFORMATION"
                send_to_telegram "${MESSAGE_DISK_SPACE}"

                send_to_slack "$MESSAGE_DISK_SPACE"
                set_variable $FREE_SPACE_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $FREE_SPACE_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $FREE_SPACE_TIME_LEFT_INDEX) minute to send mesage with disk space report."
                let VAR=$(get_variable $FREE_SPACE_TIME_LEFT_INDEX)-1
                set_variable $FREE_SPACE_TIME_LEFT_INDEX $VAR
            fi
            set_variable $IS_FREE_SPACE_ALERT_INDEX "true"
        else
            if [[ $(get_variable $IS_FREE_SPACE_ALERT_INDEX) = "true" ]]; then
                MESSAGE_DISK_SPACE="*ENOUGH FREE DISK SPACE*,
        Limit disk space: $LIMIT_DISK_SPACE Mb,
        Free disk space: $FREE_DISK_SPACE Mb,
        $ADDITIONAL_INFORMATION"
                send_to_telegram "${MESSAGE_DISK_SPACE}"

                send_to_slack "$MESSAGE_DISK_SPACE"
                set_variable $FREE_SPACE_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable $IS_FREE_SPACE_ALERT_INDEX "false"
            else
                echo "[$DAY] Enough free disk space. Free disk space: $FREE_DISK_SPACE Mb. Limit disk space $LIMIT_DISK_SPACE Mb."
            fi
        fi
    else
         echo "[$DAY] Disk space check is disabled."
    fi
}

check_cpu() {
    if [[ $(get_variable $CPU_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $CPU_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_CPU = true ]]; then
        echo "Don't worry. It collects average CPU load for $HOW_MANY_SECONDS_TO_COLLECT_CPU_AVERAGE seconds."
        CPU_PERCENT_IDLE=$(printf '%.*f\n' 0 $(sar 1 $HOW_MANY_SECONDS_TO_COLLECT_CPU_AVERAGE | grep Average | tr -s ' ' | cut -d' ' -f8))
        CPU_LOAD_AVERAGE=$(echo "100-$CPU_PERCENT_IDLE" | bc)
        if [[ $CPU_LOAD_AVERAGE -gt $LIMIT_CPU_LOAD_AVERAGE ]]; then
            if [[ $(get_variable $CPU_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                MESSAGE_CPU_LOAD="*CPU OVERLOAD*
        Average CPU load: $CPU_LOAD_AVERAGE '/,
        Limit CPU load: $LIMIT_CPU_LOAD_AVERAGE '/,
        $ADDITIONAL_INFORMATION"
                send_to_telegram "${MESSAGE_CPU_LOAD}"

                send_to_slack "$MESSAGE_CPU_LOAD"
                set_variable $CPU_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $CPU_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $CPU_TIME_LEFT_INDEX) minute to send mesage with CPU report."
                let VAR=$(get_variable $CPU_TIME_LEFT_INDEX)-1
                set_variable $CPU_TIME_LEFT_INDEX $VAR
            fi
            set_variable $IS_CPU_ALERT_INDEX "true"
        else
            if [[ $(get_variable $IS_CPU_ALERT_INDEX) = "true" ]]; then
                MESSAGE_CPU_LOAD="*Enough CPU capabilities*.
        CPU load average: ${CPU_LOAD_AVERAGE}'/,
        Limit CPU load average: ${LIMIT_CPU_LOAD_AVERAGE}'/,
        $ADDITIONAL_INFORMATION"
                send_to_telegram "${MESSAGE_CPU_LOAD}"

                send_to_slack "$MESSAGE_CPU_LOAD"
                set_variable $CPU_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable $IS_CPU_ALERT_INDEX "false"
            else
                echo "[$DAY] Enough CPU capabilities. CPU load average: ${CPU_LOAD_AVERAGE}%. Limit CPU load average: ${LIMIT_CPU_LOAD_AVERAGE}%."
            fi
        fi
    else
         echo "[$DAY] CPU check is disabled."
    fi
}

check_database() {
    if [[ $(get_variable $DATABASE_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $DATABASE_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_DATABASE = true ]]; then
        pid=$(ps aux | grep hsqldb | grep java | tr -s ' ' | cut -d' ' -f2)
        if [[ -z $pid ]]; then
            if [[ $(get_variable $DATABASE_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                MESSAGE="*DATABASE IS DOWN*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $DATABASE_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $DATABASE_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $DATABASE_TIME_LEFT_INDEX) minute to send mesage with database report."
                let VAR=$(get_variable $DATABASE_TIME_LEFT_INDEX)-1
                set_variable $DATABASE_TIME_LEFT_INDEX $VAR
            fi
            set_variable $IS_DATABASE_ALERT_INDEX "true"
        else
            if [[ $(get_variable $IS_DATABASE_ALERT_INDEX) = "true" ]]; then
                MESSAGE="*DATABASE IS UP*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $DATABASE_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable $IS_DATABASE_ALERT_INDEX "false"
            else
                echo "[$DAY] DataBase is UP."
            fi
        fi
    else
         echo "[$DAY] DataBase check is disabled."
    fi
}

check_backend() {
    if [[ $(get_variable $BACKEND_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $BACKEND_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_BACKEND = true ]]; then
        pid=$(ps aux | grep [w]ebapp\.server\.ServerMain | tr -s ' ' | cut -d' ' -f2)
        if [[ -z $pid ]]; then
            if [[ $(get_variable $BACKEND_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                MESSAGE="*BACKEND IS DOWN*,
        $ADDITIONAL_INFORMATION"

                end_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $BACKEND_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $BACKEND_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $BACKEND_TIME_LEFT_INDEX) minute to send mesage with backend report."
                let VAR=$(get_variable $BACKEND_TIME_LEFT_INDEX)-1
                set_variable $BACKEND_TIME_LEFT_INDEX $VAR
            fi
            set_variable $IS_BACKEND_ALERT_INDEX "true"
        else
            if [[ $(get_variable $IS_BACKEND_ALERT_INDEX) = "true" ]]; then
                MESSAGE="*BACKEND IS UP*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $BACKEND_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable $IS_BACKEND_ALERT_INDEX "false"
            else
                echo "[$DAY] BackEnd is UP."
            fi
        fi
    else
        echo "[$DAY] BackEnd check is disabled."
    fi
}

check_pechkin() {
    if [[ $(get_variable $PECHKIN_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $PECHKIN_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_PECHKIN = true ]]; then
        pid=$(ps aux | grep [m]icroservices\.pechkin\.PechkinServer | tr -s ' ' | cut -d' ' -f2)
        if [[ -z $pid ]]; then
            if [[ $(get_variable $PECHKIN_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                MESSAGE="*PECHKIN IS DOWN*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $PECHKIN_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $PECHKIN_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $PECHKIN_TIME_LEFT_INDEX) minute to send mesage with pechkin report."
                let VAR=$(get_variable $PECHKIN_TIME_LEFT_INDEX)-1
                set_variable $PECHKIN_TIME_LEFT_INDEX $VAR
            fi
            set_variable $IS_PECHKIN_ALERT_INDEX "true"
        else
            if [[ $(get_variable $IS_PECHKIN_ALERT_INDEX) = "true" ]]; then
                MESSAGE="*PECHKIN IS UP*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $PECHKIN_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable $IS_PECHKIN_ALERT_INDEX "false"
            else
                echo "[$DAY] Pechkin is UP."
            fi
        fi
    else
         echo "[$DAY] Pechkin check is disabled."
    fi
}

check_autochartist() {
    if [[ $(get_variable $AUTOCHARTIST_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $AUTOCHARTIST_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_AUTOCHARTIST = true ]]; then
        pid=$(ps aux | grep [m]icroservices\.autochartist\.AutochartistServer | tr -s ' ' | cut -d' ' -f2)
        if [[ -z $pid ]]; then
            if [[ $(get_variable $AUTOCHARTIST_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                echo ""
                MESSAGE="*AUTOCHARTIST IS DOWN*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $AUTOCHARTIST_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $AUTOCHARTIST_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $AUTOCHARTIST_TIME_LEFT_INDEX) minute to send mesage with autochartist report."
                let VAR=$(get_variable $AUTOCHARTIST_TIME_LEFT_INDEX)-1
                set_variable $AUTOCHARTIST_TIME_LEFT_INDEX $VAR
            fi
            set_variable $IS_AUTOCHARTIST_ALERT_INDEX "true"
        else
            if [[ $(get_variable $IS_AUTOCHARTIST_ALERT_INDEX) = "true" ]]; then
                echo ""
                MESSAGE="*AUTOCHARTIST IS UP*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $AUTOCHARTIST_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable $IS_AUTOCHARTIST_ALERT_INDEX "false"
            else
                echo "[$DAY] Autochartist is UP."
            fi
        fi
    else
         echo "[$DAY] Autochartist check is disabled."
    fi
}

check_api() {
    if [[ $(get_variable $API_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $API_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_API = true ]]; then
        pid=$(ps aux | grep [m]icroservices\.api\.ApiServer | tr -s ' ' | cut -d' ' -f2)
        if [[ -z $pid ]]; then
            if [[ $(get_variable $API_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                echo ""
                MESSAGE="*API IS DOWN*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $API_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $API_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $API_TIME_LEFT_INDEX) minute to send mesage with API report"
                let VAR=$(get_variable $API_TIME_LEFT_INDEX)-1
                set_variable $API_TIME_LEFT_INDEX $VAR
            fi
            set_variable $IS_API_ALERT_INDEX "true"
        else
            if [[ $(get_variable $IS_API_ALERT_INDEX) = "true" ]]; then
                echo ""
                MESSAGE="*API IS UP*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $API_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable $IS_API_ALERT_INDEX "false"
            else
                echo "[$DAY] API is UP."
            fi
        fi
    else
        echo "[$DAY] API check is disabled."
    fi
}

check_rabbit() {
    if [[ $(get_variable $RABBIT_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $RABBIT_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_RABBIT = true ]]; then
        pid=$(ps aux | grep etc/rabbitmq | grep rabbitmq_server | tr -s ' ' | cut -d' ' -f2)
        if [[ -z $pid ]]; then
            if [[ $(get_variable $RABBIT_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                echo ""
                MESSAGE="*RABBIT IS DOWN*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $RABBIT_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $RABBIT_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $RABBIT_TIME_LEFT_INDEX) minute to send mesage with rabbit raport."
                let VAR=$(get_variable $RABBIT_TIME_LEFT_INDEX)-1
                set_variable $RABBIT_TIME_LEFT_INDEX $VAR
            fi
            set_variable $IS_RABBIT_ALERT_INDEX "true"
        else
            if [[ $(get_variable $IS_RABBIT_ALERT_INDEX) = "true" ]]; then
                echo ""
                MESSAGE="*RABBIT IS UP*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $RABBIT_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable $IS_RABBIT_ALERT_INDEX "false"
            else
                echo "[$DAY] Rabbit is UP."
            fi
        fi
    else
         echo "[$DAY] Rabbit check is disabled."
    fi
}

check_nginx() {
    if [[ $(get_variable $NGINX_TIME_LEFT_INDEX) -eq 0 ]]; then
        set_variable $NGINX_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
    fi
    if [[ $IS_CHECK_NGINX = true ]]; then
        pid=$(ps aux | grep usr/sbin/nginx | grep master | tr -s ' ' | cut -d' ' -f2)
        if [[ -z $pid ]]; then
            if [[ $(get_variable $NGINX_TIME_LEFT_INDEX) -eq $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND ]]; then
                echo ""
                MESSAGE="*NGINX IS DOWN*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $NGINX_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
            fi
            if [[ $(get_variable $NGINX_TIME_LEFT_INDEX) != "0" ]]; then
                echo "$(get_variable $NGINX_TIME_LEFT_INDEX) minute to send mesage with nginx report."
                let VAR=$(get_variable $NGINX_TIME_LEFT_INDEX)-1
                set_variable $NGINX_TIME_LEFT_INDEX $VAR
            fi
            set_variable "$IS_NGINX_ALERT_INDEX" "true"
        else
            if [[ $(get_variable $IS_NGINX_ALERT_INDEX) = "true" ]]; then
                echo ""
                MESSAGE="*NGINX IS UP*,
        $ADDITIONAL_INFORMATION"

                send_to_telegram "${MESSAGE}"

                send_to_slack "$MESSAGE"
                set_variable $NGINX_TIME_LEFT_INDEX $DEFAULT_WAIT_MINUTE_FOR_NEXT_SEND
                set_variable "$IS_NGINX_ALERT_INDEX" "false"
            else
                echo "[$DAY] NGINX is UP."
            fi
        fi
    else
         echo "[$DAY] NGINX check is disabled."
    fi
}

### main ###
check_memory
check_free_space
check_cpu
check_database
check_backend
check_pechkin
check_autochartist
check_api
check_rabbit
check_nginx

exit 0

